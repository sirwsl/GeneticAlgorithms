package generation;

import java.util.*;
import java.util.stream.Collectors;

public class QuestionService {

    /**
     *
     * @param type 1 单选  2 填空  3 主观
     * @param substring ： 集合
     * @return
     */
    public static QuestionBean[] getQuestionArray(int type, String substring) {
        List<String> list = Arrays.asList(substring.split(","));
        List<QuestionBean> collect = getAll().stream().filter(li -> li.getType() == type && list.contains(String.valueOf(li.getPointId()))).collect(Collectors.toList());
        return collect.toArray(new QuestionBean[collect.size()]);
    }

    /**
     * 类型一样，分数相同
     * @param tmpQuestion
     * @return
     */
    public static List<QuestionBean> getQuestionListWithOutSId(QuestionBean tmpQuestion) {
        List<QuestionBean> collect = getAll().stream().filter(li -> li.getType() == tmpQuestion.getType() && tmpQuestion.getScore() == li.getScore()).collect(Collectors.toList());
        return collect;
    }

    public static List<QuestionBean> getAll(){
        List <QuestionBean> questionBeanList = new ArrayList<>(1024);
        String[] answer = {"A","B","C","D","AB","AC","AD","BC","BD","CD","ABC","BCD","ABCD"};
        String[] knowledge = {"知识点AA","知识点BB","知识点CC","知识点DD","知识点EE","知识点FF","知识点GG","知识点HH","知识点II","知识点JJ","知识点KK"};
        Map<Long,String> user = new HashMap<>();
        Long[] userId = {11111111L,22222222L,66666666L,88888888L};
        user.put(11111111L,"张三");
        user.put(22222222L,"李四");
        user.put(66666666L,"王五");
        user.put(88888888L,"赵六");
        Random random = new Random();
        for (int i = 0; i< 2048; i++){
            QuestionBean que = new QuestionBean();
            que.setAnswer(answer[random.nextInt(12)]);
            que.setChoice1("第"+i+"题选项A");
            que.setChoice2("第"+i+"题选项B");
            que.setChoice3("第"+i+"题选项C");
            que.setChoice4("第"+i+"题选项D");
            que.setId(i);
            que.setScore(random.nextInt(5));
            que.setCreateTime(new Date(2022,03,01));
            java.text.DecimalFormat df =new java.text.DecimalFormat("#.0");
            double v = (float) (3 + random.nextInt(7)) / 10.0;
            String s = df.format(v);
            que.setDifficulty(Double.parseDouble(s));
            int kid = random.nextInt(10);
            que.setKnowledgeName(knowledge[kid]);
            que.setPointId(kid);
            que.setStem("这是第 "+i+" 题题目，题目："+i);
            que.setType(random.nextInt(3)+1);
            Long uid = userId[random.nextInt(4)];
            que.setUserId(uid);
            que.setUserName(user.get(uid));
            questionBeanList.add(que);
        }
        return questionBeanList;

    }
}
