package generation;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 测试类
 */
public class Main {
    public static void main(String[] args) {
        Paper resultPaper = null;
        // 迭代计数器
        int count = 0;
        int runCount = 100;
        // 适应度期望值
        double expand = 0.98;

        // 可自己初始化组卷规则rule
        RuleBean rule = new RuleBean();
        rule.setId(10);
        rule.setExamId(101);
        //题目数量
        rule.setCompleteNum(20);
        rule.setCompleteScore(2);
        rule.setSingleNum(20);
        rule.setSingleScore(2);
        rule.setSubjectiveNum(4);
        rule.setSubjectiveScore(5);
        //知识点范围
        rule.setPointIds("2,3,4,5,6");
        //难度
        rule.setDifficulty(0.8);
        //总分
        rule.setTotalMark(100);
        //创建时间
        rule.setCreateTime(rule.getCreateTime());

        // 初始化种群
        Population population = new Population(20, true, rule);
        System.out.println("初次适应度  " + population.getFitness().getAdaptationDegree());
        while (count < runCount && population.getFitness().getAdaptationDegree() < expand) {
            count++;
            //种群进化
            population = GA.evolvePopulation(population, rule);
            System.out.println("第 " + count + " 次进化，适应度为： " + population.getFitness().getAdaptationDegree());
        }
        System.out.println("进化次数： " + count);
        System.out.println(population.getFitness().getAdaptationDegree());
        //获取最优解
        resultPaper = population.getFitness();
        System.out.println(JSONObject.toJSON(resultPaper));
    }
}
